unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.StdCtrls,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Memo, FMX.Edit, FMX.Objects,
  FMX.DateTimeCtrls, FMX.ListBox;

type
  TFLotoDelphi = class(TForm)
    Button1: TButton;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    CheckBox10: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox17: TCheckBox;
    CheckBox18: TCheckBox;
    CheckBox19: TCheckBox;
    CheckBox20: TCheckBox;
    CheckBox21: TCheckBox;
    CheckBox22: TCheckBox;
    CheckBox23: TCheckBox;
    CheckBox24: TCheckBox;
    CheckBox25: TCheckBox;
    CheckBox26: TCheckBox;
    CheckBox27: TCheckBox;
    CheckBox28: TCheckBox;
    CheckBox29: TCheckBox;
    CheckBox30: TCheckBox;
    CheckBox31: TCheckBox;
    CheckBox32: TCheckBox;
    CheckBox33: TCheckBox;
    CheckBox34: TCheckBox;
    CheckBox35: TCheckBox;
    CheckBox36: TCheckBox;
    CheckBox37: TCheckBox;
    CheckBox38: TCheckBox;
    Line1: TLine;
    Circle1: TCircle;
    Edit1: TLabel;
    Circle2: TCircle;
    Edit2: TLabel;
    Circle3: TCircle;
    Edit3: TLabel;
    Circle4: TCircle;
    Edit4: TLabel;
    Circle5: TCircle;
    Edit5: TLabel;
    Circle6: TCircle;
    Edit6: TLabel;
    Circle7: TCircle;
    Edita1: TLabel;
    Circle8: TCircle;
    Edita2: TLabel;
    Circle9: TCircle;
    Edita3: TLabel;
    Circle10: TCircle;
    Edita4: TLabel;
    Circle11: TCircle;
    Edita5: TLabel;
    Circle12: TCircle;
    Edita6: TLabel;
    Circle13: TCircle;
    Editb1: TLabel;
    Circle14: TCircle;
    Editb2: TLabel;
    Circle15: TCircle;
    editb3: TLabel;
    Circle16: TCircle;
    Editb4: TLabel;
    Circle17: TCircle;
    Editb5: TLabel;
    Circle18: TCircle;
    Editb6: TLabel;
    Circle19: TCircle;
    Editc1: TLabel;
    Circle20: TCircle;
    Editc2: TLabel;
    Circle21: TCircle;
    Editc3: TLabel;
    Circle22: TCircle;
    Editc4: TLabel;
    Circle23: TCircle;
    Editc5: TLabel;
    Circle24: TCircle;
    Editc6: TLabel;
    Circle25: TCircle;
    Edit7: TLabel;
    Circle26: TCircle;
    Edita7: TLabel;
    Circle27: TCircle;
    Editb7: TLabel;
    Circle28: TCircle;
    Editc7: TLabel;
    Text1: TText;
    Button2: TButton;
    Text2: TText;
    Text3: TText;
    ComboBox1: TComboBox;
    ToolBar1: TToolBar;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure inventarSorteo;
  public
    { Public declarations }
  end;

var
  FLotoDelphi: TFLotoDelphi;

implementation

uses LogoGen;

{$R *.fmx}

{ TForm2 }

procedure TFLotoDelphi.Button1Click(Sender: TObject);
begin
   inventarSorteo;
end;

procedure TFLotoDelphi.FormCreate(Sender: TObject);
var i : integer;
begin
  for i := 0 to componentCount -1 do
   if Components[i] is TEdit then
     TEdit(Components[i]).Text := ''
    else
   if Components[i] is TLabel then
     TLabel(Components[i]).Text := ''
end;

procedure TFLotoDelphi.inventarSorteo;
var S: TSorteo;
begin
  Randomize;
  S := TSorteo.Create;

  if CheckBox1.IsChecked then
     S.Excluir( '01' );
  if CheckBox2.IsChecked then
     S.Excluir( '02' );
  if CheckBox3.IsChecked then
     S.Excluir( '03' );
  if CheckBox4.IsChecked then
     S.Excluir( '04' );
  if CheckBox5.IsChecked then
     S.Excluir( '05' );
  if CheckBox6.IsChecked then
     S.Excluir( '06' );
  if CheckBox7.IsChecked then
     S.Excluir( '07' );
  if CheckBox8.IsChecked then
     S.Excluir( '08' );
  if CheckBox9.IsChecked then
     S.Excluir( '09' );
  if CheckBox10.IsChecked then
     S.Excluir( '10' );
  if CheckBox11.IsChecked then
     S.Excluir( '11' );
  if CheckBox12.IsChecked then
     S.Excluir( '12' );
  if CheckBox13.IsChecked then
     S.Excluir( '13' );
  if CheckBox14.IsChecked then
     S.Excluir( '14' );
  if CheckBox15.IsChecked then
     S.Excluir( '15' );
  if CheckBox16.IsChecked then
     S.Excluir( '16' );
  if CheckBox17.IsChecked then
     S.Excluir( '17' );
  if CheckBox18.IsChecked then
     S.Excluir( '18' );
  if CheckBox19.IsChecked then
     S.Excluir( '19' );
  if CheckBox20.IsChecked then
     S.Excluir( '20' );
  if CheckBox21.IsChecked then
     S.Excluir( '21' );
  if CheckBox22.IsChecked then
     S.Excluir( '22' );
  if CheckBox23.IsChecked then
     S.Excluir( '23' );
  if CheckBox24.IsChecked then
     S.Excluir( '24' );
  if CheckBox25.IsChecked then
     S.Excluir( '25' );
  if CheckBox26.IsChecked then
     S.Excluir( '26' );
  if CheckBox27.IsChecked then
     S.Excluir( '27' );
  if CheckBox28.IsChecked then
     S.Excluir( '28' );
  if CheckBox29.IsChecked then
     S.Excluir( '29' );
  if CheckBox30.IsChecked then
     S.Excluir( '30' );
  if CheckBox31.IsChecked then
     S.Excluir( '31' );
  if CheckBox32.IsChecked then
     S.Excluir( '32' );
  if CheckBox33.IsChecked then
     S.Excluir( '33' );
  if CheckBox34.IsChecked then
     S.Excluir( '34' );
  if CheckBox35.IsChecked then
     S.Excluir( '35' );
  if CheckBox36.IsChecked then
     S.Excluir( '36' );
  if CheckBox37.IsChecked then
     S.Excluir( '37' );
  if CheckBox38.IsChecked then
     S.Excluir( '38' );

  S.Inventar;
  Edit1.Text:= S.Numeros[0];
  Edit2.Text:= S.Numeros[1];
  Edit3.Text:= S.Numeros[2];
  Edit4.Text:= S.Numeros[3];
  Edit5.Text:= S.Numeros[4];
  Edit6.Text:= S.Numeros[5];
  Edit7.Text:= (Random(9) + 1).ToString;

  S.Inventar;
  Edita1.Text:= S.Numeros[0];
  Edita2.Text:= S.Numeros[1];
  Edita3.Text:= S.Numeros[2];
  Edita4.Text:= S.Numeros[3];
  Edita5.Text:= S.Numeros[4];
  Edita6.Text:= S.Numeros[5];
  Edita7.Text:= (Random(9) + 1).ToString;

  S.Inventar;
  Editb1.Text:= S.Numeros[0];
  Editb2.Text:= S.Numeros[1];
  Editb3.Text:= S.Numeros[2];
  Editb4.Text:= S.Numeros[3];
  Editb5.Text:= S.Numeros[4];
  Editb6.Text:= S.Numeros[5];
  Editb7.Text:= (Random(9) + 1).ToString;

  S.Inventar;
  Editc1.Text:= S.Numeros[0];
  Editc2.Text:= S.Numeros[1];
  Editc3.Text:= S.Numeros[2];
  Editc4.Text:= S.Numeros[3];
  Editc5.Text:= S.Numeros[4];
  Editc6.Text:= S.Numeros[5];
  Editc7.Text:= (Random(9) + 1).ToString;

  S.Free;
end;

end.
